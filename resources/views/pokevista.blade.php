@extends('plantilla')
@section('titulo')
    -Listado
@endsection
@section('principal')
@if($mensaje = Session::get('success'))
<div class="row" id="divok">
    <div class="col-md-6 offset-md-3">
        <div class="alert alert-success">
            {{$mensaje}}
        </div>
    </div>
</div>
@endif
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>NOMBRE</th>
                                <th>TIPO</th>
                                <th>HABILIDADES</th>
                                <th>POKEDEX</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pokemones as $i => $row)
                                <tr>
                                    <td>{{($i+1)}}</td>
                                    <td>{{$row->nombre}}</td>
                                    <td>{{$row->tipo}}</td>
                                    <td>{{$row->habilidades}}</td>
                                    <td>{{$row->pokedex}}</td>
                                    <td>
                                        <a class="btn btn-success" href="{{ route('pokemones.edit',$row->id)}}">
                                            <i class="fa-solid fa-edit"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <form method="POST" id="frm_{{$row->id}}" action="{{route('pokemones.destroy',$row->id)}}">
                                            @csrf
                                            @method('DELETE')
                                            <button  class="btn btn-danger" onclick="datos('{{$row->id}}','{{$row->nombre}}')" type="button" data-bs-toggle="modal" data-bs-target="#modalEliminar"><i class="fa-solid fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" id="modalEliminar">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Eliminar</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <p>¿Seguro Que Quieres Elimar El Pokemon #<label id="lbl_nombre"></label>?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="fa-solid fa-ban"></i> Ñao Ñao</button>
              <button id="btnEliminar" type="button" class="btn btn-success"><i class="fa-solid fa-check"></i> Si, pokeliminalo</button>
            </div>
          </div>
        </div>
      </div>
@endsection
@section('js')
@vite('resources/js/listado.js')
@endsection